package lelasystems.com.br.desafioandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import lelasystems.com.br.desafioandroid.R;
import lelasystems.com.br.desafioandroid.activities.MainActivity;
import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.models.PullRequest;
import lelasystems.com.br.desafioandroid.models.PullRequestList;
import lelasystems.com.br.desafioandroid.server.WebClient;
import lelasystems.com.br.desafioandroid.server.WebClientPull;

import java.util.ArrayList;
import java.util.List;


public class PullRequestFragment extends Fragment {

    public Context ctx;
    public RecyclerView recyclerView;
    public InformationService iService;
    public PullRequestList pulls;
    public ArrayList<PullRequest> pullsRequests = new ArrayList<>();
    public TextView txtvOpened;
    public TextView txtvClosed;
    public TextView txtvRepositoryName;
    public ImageView imgvBack;
    public Fragment frag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pullrequest_list, container, false);

        ctx = view.getContext();
        frag = this;

        txtvOpened = (TextView) view.findViewById(R.id.txtvOpened);
        txtvClosed = (TextView) view.findViewById(R.id.txtvClosed);
        txtvRepositoryName = (TextView) view.findViewById(R.id.txtvRepositoryNameTitle);
        imgvBack = (ImageView) view.findViewById(R.id.imgvBack);

        imgvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        Bundle b = getArguments();
        if(b != null){
            iService = (InformationService) b.getSerializable("iService");
        }
        txtvRepositoryName.setText(iService.getRepositoryName());
        recyclerView = (RecyclerView) view.findViewById(R.id.list2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new PullRequestRecyclerViewAdapter(pullsRequests));

        new WebClientPull().getPull(ctx, iService);

        return view;
    }

    public void populateList(PullRequestList pulls) {
        this.pullsRequests.addAll(pulls.getAllPullRequests());
        txtvClosed.setText(""+pulls.getClosed() + " closed");
        txtvOpened.setText(""+pulls.getOpened() + " opened");
        recyclerView.getAdapter().notifyDataSetChanged();
    }
}
