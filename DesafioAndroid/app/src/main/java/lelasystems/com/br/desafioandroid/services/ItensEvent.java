package lelasystems.com.br.desafioandroid.services;

/**
 * Created by lela on 08/05/17.
 */

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import lelasystems.com.br.desafioandroid.models.InformationService;

public class ItensEvent {
    public ArrayList<InformationService> itens;

    public ItensEvent(ArrayList<InformationService> itens){
        this.itens = itens;
    }
}
