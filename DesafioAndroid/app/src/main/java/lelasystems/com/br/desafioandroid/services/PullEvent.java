package lelasystems.com.br.desafioandroid.services;

/**
 * Created by lela on 08/05/17.
 */

import android.util.Log;

import java.util.ArrayList;

import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.models.PullRequestList;

public class PullEvent {
    public PullRequestList pull;

    public PullEvent(PullRequestList pull){
        this.pull = pull;
    }
}
