package lelasystems.com.br.desafioandroid.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lelasystems.com.br.desafioandroid.custom.Config;
import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.models.PullRequest;
import lelasystems.com.br.desafioandroid.models.PullRequestList;
import lelasystems.com.br.desafioandroid.services.ErroEvent;
import lelasystems.com.br.desafioandroid.services.ItensEvent;
import lelasystems.com.br.desafioandroid.services.PullEvent;

/**
 * Created by android6596 on 01/02/17.
 */

public class WebClientPull {

    public Context ctx;
    public ArrayList<InformationService> allRepositories;
    public static String TAG = "WebClientPull";
    public ProgressDialog pDialog;
    public int page;
    public int opened = 0;
    public int closed = 0;
    public ArrayList<PullRequest> allPullRequests;

    public WebClientPull() {
    }

    public void getPull(Context ctx, InformationService iService) {
        this.page = page;
        this.ctx = ctx;

        RequestQueue queue = Volley.newRequestQueue(ctx);
        String url = Config.pullRequestsUrl + iService.getUserName() + "/" + iService.getRepositoryName() + "/pulls";

        JsonArrayRequest jsArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                saveJsonArray(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Response error = " + error);
                pDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String credentials = "leandro.spidalieri@gmail.com:erun2pbr";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }};

        queue.add(jsArrayRequest);
        pDialog = new ProgressDialog(ctx);
        pDialog.setMessage("Buscando pull requests....");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
    }

    public void saveJsonArray(JSONArray jsonArray) {
        try {
            allPullRequests = new ArrayList<>();
            opened = 0;
            closed = 0;
            if (jsonArray != null) {
                for (int i2 = 0; i2 < jsonArray.length(); i2++) {
                    final PullRequest pRequest = new PullRequest();
                    pRequest.setTitle(jsonArray.getJSONObject(i2).getString("title"));
                    pRequest.setBody(jsonArray.getJSONObject(i2).getString("body"));
                    JSONObject user = jsonArray.getJSONObject(i2).getJSONObject("user");
                    pRequest.setUserName(user.getString("login"));
                    pRequest.setHtmlUrl(jsonArray.getJSONObject(i2).getString("html_url"));
                    RequestQueue queue2 = Volley.newRequestQueue(ctx);
                    String url2 = user.getString("url");


                    JsonObjectRequest jsObjRequest2 = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pRequest.setFullName(response.getString("name"));
                            } catch (JSONException e) {
                                Log.e(TAG, "JSON Execption = " + e);
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Response error = " + error);

                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            String credentials = "leandro.spidalieri@gmail.com:erun2pbr";
                            String auth = "Basic "
                                    + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                            headers.put("Content-Type", "application/json");
                            headers.put("Authorization", auth);
                            return headers;
                        }
                    };

                    queue2.add(jsObjRequest2);
                    pRequest.setUserImage(user.getString("avatar_url"));
                    allPullRequests.add(pRequest);
                    if (jsonArray.getJSONObject(i2).getString("state").equals("open")) {
                        opened = opened + 1;
                    } else {
                        closed = closed + 1;
                    }
                }
            } else {
                Log.e(TAG, "JSONArray null");
            }

            PullRequestList pList = new PullRequestList();
            pList.setAllPullRequests(allPullRequests);
            pList.setOpened(opened);
            pList.setClosed(closed);
            pDialog.dismiss();
            EventBus.getDefault().post(new PullEvent(pList));


        } catch (Exception e) {
            Log.e(TAG, "Execption = " + e);
        }
    }
}
