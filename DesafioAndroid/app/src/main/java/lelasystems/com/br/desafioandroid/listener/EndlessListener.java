package lelasystems.com.br.desafioandroid.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by android6596 on 02/02/17.
 */
public abstract class EndlessListener extends RecyclerView.OnScrollListener {

    public int totalBefore = 0;
    public boolean loading = true;
    int qtdTotalItems;
    int firstItemVisible;
    int qtdVisibleItems;
    int indLimitToLoad;

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        qtdTotalItems = layoutManager.getItemCount();
        firstItemVisible = layoutManager.findFirstVisibleItemPosition();
        qtdVisibleItems = recyclerView.getChildCount();
        indLimitToLoad = qtdTotalItems - qtdVisibleItems - 15;

        if(loading) {
            if (qtdTotalItems > totalBefore) {
                loading = false;
                totalBefore = qtdTotalItems;
            }
        }

        if(!loading && firstItemVisible >= indLimitToLoad){
            loadMore();
            loading = true;
        }
    }

    public abstract void loadMore();

}
