package lelasystems.com.br.desafioandroid.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import lelasystems.com.br.desafioandroid.R;
import lelasystems.com.br.desafioandroid.fragments.GhJavaPopFragment;
import lelasystems.com.br.desafioandroid.fragments.PullRequestFragment;
import lelasystems.com.br.desafioandroid.interfaces.PullDelegate;
import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.models.PullRequest;
import lelasystems.com.br.desafioandroid.models.PullRequestList;
import lelasystems.com.br.desafioandroid.server.WebClient;
import lelasystems.com.br.desafioandroid.services.ErroEvent;
import lelasystems.com.br.desafioandroid.services.ItensEvent;
import lelasystems.com.br.desafioandroid.services.PullEvent;

public class MainActivity extends AppCompatActivity implements PullDelegate{

    public GhJavaPopFragment ghJavaPopFragment;
    public PullRequestFragment fragmentPull;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ghJavaPopFragment = new GhJavaPopFragment();
        transaction.replace(R.id.frame_principal, ghJavaPopFragment);
        transaction.commit();

        new WebClient().getRList(1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onSucess(ItensEvent event) {

        ArrayList<InformationService> itens = event.itens;
        ghJavaPopFragment.populateList(itens);
    }

    @Subscribe
    public void onError(ErroEvent event) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setMessage("Esse aplicativo necessita de conexão")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert2 = alertDialogBuilder.create();
        alert2.show();
    }

    @Override
    public void selectPull(InformationService iService) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putSerializable("iService", iService);
        fragmentPull = new PullRequestFragment();
        fragmentPull.setArguments(b);
        transaction.replace(R.id.frame_principal, fragmentPull);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Subscribe
    public void onSucessPulls(PullEvent event) {

        PullRequestList pulls = event.pull;
        fragmentPull.populateList(pulls);
    }



}
