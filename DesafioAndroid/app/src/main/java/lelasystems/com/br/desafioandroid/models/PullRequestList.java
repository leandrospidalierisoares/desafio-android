package lelasystems.com.br.desafioandroid.models;

import java.util.ArrayList;

public class PullRequestList {

    public ArrayList<PullRequest> allPullRequests;
    public int opened;
    public int closed;

    public ArrayList<PullRequest> getAllPullRequests() {
        return allPullRequests;
    }

    public void setAllPullRequests(ArrayList<PullRequest> allPullRequests) {
        this.allPullRequests = allPullRequests;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int opened) {
        this.opened = opened;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }
}
