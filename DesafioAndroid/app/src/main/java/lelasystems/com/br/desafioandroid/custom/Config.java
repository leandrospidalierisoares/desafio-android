package lelasystems.com.br.desafioandroid.custom;

/**
 * Created by lela on 20/08/16.
 */

public class Config {

    public static String repositoriesUrl = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";
    public static String pullRequestsUrl = "https://api.github.com/repos/";
}
