package lelasystems.com.br.desafioandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lelasystems.com.br.desafioandroid.R;
import lelasystems.com.br.desafioandroid.listener.EndlessListener;
import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.server.WebClient;

import java.util.ArrayList;

public class GhJavaPopFragment extends Fragment {

    public ArrayList<InformationService> rList = new ArrayList<>();
    public RecyclerView recyclerView;
    public Context ctx;
    public int i = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ghjavapop_list, container, false);

        ctx = view.getContext();

        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setAdapter(new GhJavaPopRecyclerViewAdapter(rList));

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnScrollListener(new EndlessListener() {
            @Override
            public void loadMore() {
                i = i + 1;
                new WebClient().getRList(i, ctx);
            }
        });

        return view;
    }

    public void populateList(ArrayList<InformationService> itens) {
        this.rList.addAll(itens);
        recyclerView.getAdapter().notifyDataSetChanged();
    }


}
