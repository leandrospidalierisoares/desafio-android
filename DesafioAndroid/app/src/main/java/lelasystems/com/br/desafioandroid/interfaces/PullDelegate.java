package lelasystems.com.br.desafioandroid.interfaces;

import lelasystems.com.br.desafioandroid.models.InformationService;

/**
 * Created by lela on 09/05/17.
 */

public interface PullDelegate {
    void selectPull(InformationService iService);
}
