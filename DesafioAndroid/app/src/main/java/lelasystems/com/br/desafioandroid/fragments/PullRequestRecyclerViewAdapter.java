package lelasystems.com.br.desafioandroid.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import lelasystems.com.br.desafioandroid.R;
import lelasystems.com.br.desafioandroid.interfaces.PullDelegate;
import lelasystems.com.br.desafioandroid.models.PullRequest;
import lelasystems.com.br.desafioandroid.models.PullRequestList;

import java.util.ArrayList;

@TargetApi(Build.VERSION_CODES.M)
public class PullRequestRecyclerViewAdapter extends RecyclerView.Adapter<PullRequestRecyclerViewAdapter.ViewHolder> {

    public ArrayList<PullRequest> mValues;
    public Context ctx;


    public PullRequestRecyclerViewAdapter(ArrayList<PullRequest> pulls) {
        mValues = pulls;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pullrequest, parent, false);
        ctx = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mTxtvPullTitle.setText(mValues.get(position).title);
        holder.mTxtvPullDescription.setText(mValues.get(position).body);
        Picasso.with(ctx)
                .load(mValues.get(position).userImage)
                .into(holder.mImgvUser);
        holder.mTxtvUserName.setText(mValues.get(position).userName);
        holder.mTxtvUserFullName.setText(mValues.get(position).fullName);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriNet = Uri.parse(mValues.get(position).getHtmlUrl());
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriNet);
                ctx.startActivity(launchBrowser);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTxtvPullTitle;
        public final TextView mTxtvPullDescription;
        public final ImageView mImgvUser;
        public final TextView mTxtvUserName;
        public final TextView mTxtvUserFullName;
        public PullRequest mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTxtvPullTitle = (TextView) itemView.findViewById(R.id.pullTitle);
            mTxtvPullDescription = (TextView) itemView.findViewById(R.id.pullDescription);
            mImgvUser = (ImageView) itemView.findViewById(R.id.imgvUserPull);
            mTxtvUserName = (TextView) itemView.findViewById(R.id.txtvUserNamePull);
            mTxtvUserFullName = (TextView) itemView.findViewById(R.id.txtvUserFullNamePull);
        }
    }
}
