package lelasystems.com.br.desafioandroid.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import lelasystems.com.br.desafioandroid.R;
import lelasystems.com.br.desafioandroid.custom.Config;
import lelasystems.com.br.desafioandroid.interfaces.PullDelegate;
import lelasystems.com.br.desafioandroid.models.InformationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TargetApi(Build.VERSION_CODES.M)
public class GhJavaPopRecyclerViewAdapter extends RecyclerView.Adapter<GhJavaPopRecyclerViewAdapter.ViewHolder> {

    private final List<InformationService> mValues;
    public Context ctx;

    public GhJavaPopRecyclerViewAdapter(ArrayList<InformationService> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ghjavapop, parent, false);
        ctx = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTxtvRepositoryName.setText(mValues.get(position).repositoryName);
        holder.mTxtvDescRepositoryName.setText(mValues.get(position).repositoryDescription);
        holder.mTxtvFork.setText("" + mValues.get(position).forks);
        holder.mTxtvStar.setText("" + mValues.get(position).stars);
        Picasso.with(ctx)
                .load(mValues.get(position).userPhoto)
                .into(holder.mImgvUser);
        holder.mTxtvUserName.setText(mValues.get(position).userName);
        holder.mTxtvUserFullName.setText(mValues.get(position).fullName);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PullDelegate delegate = (PullDelegate) holder.mView.getContext();
                delegate.selectPull(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTxtvRepositoryName;
        public final TextView mTxtvDescRepositoryName;
        public final TextView mTxtvFork;
        public final TextView mTxtvStar;
        public final ImageView mImgvUser;
        public final TextView mTxtvUserName;
        public final TextView mTxtvUserFullName;
        public InformationService mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTxtvRepositoryName = (TextView) itemView.findViewById(R.id.txtvRepositoryName);
            mTxtvDescRepositoryName = (TextView) itemView.findViewById(R.id.txtvDescRepositoryName);
            mTxtvFork = (TextView) itemView.findViewById(R.id.txtvFork);
            mTxtvStar = (TextView) itemView.findViewById(R.id.txtvStar);
            mImgvUser = (ImageView) itemView.findViewById(R.id.imgvUser);
            mTxtvUserName = (TextView) itemView.findViewById(R.id.txtvUserName);
            mTxtvUserFullName = (TextView) itemView.findViewById(R.id.txtvUserFullName);
        }

    }
}




