package lelasystems.com.br.desafioandroid.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lelasystems.com.br.desafioandroid.activities.MainActivity;
import lelasystems.com.br.desafioandroid.custom.Config;
import lelasystems.com.br.desafioandroid.models.InformationService;
import lelasystems.com.br.desafioandroid.services.ItensEvent;
import lelasystems.com.br.desafioandroid.services.ErroEvent;

/**
 * Created by android6596 on 01/02/17.
 */

public class WebClient {

    public Context ctx;
    public ArrayList<InformationService> allRepositories;
    public static String TAG = "WebClient";
    public ProgressDialog pDialog;
    public int page;

    public WebClient() {
    }

    public void getRList(int page, Context ctx) {
        this.page = page;
        this.ctx = ctx;

        allRepositories = new ArrayList<>();

        RequestQueue queue = Volley.newRequestQueue(ctx);
        String url = Config.repositoriesUrl + page;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                saveJsonObject(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                EventBus.getDefault().post(new ErroEvent(error.getMessage()));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String credentials = "leandro.spidalieri@gmail.com:erun2pbr";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        queue.add(jsObjRequest);
        if(page == 1) {
            pDialog = new ProgressDialog(ctx);
            pDialog.setMessage("Buscando....");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
    }

    public void saveJsonObject(JSONObject response) {
        try {
            if (response != null) {
                JSONArray jsonArray = response.getJSONArray("items");
                for (int i2 = 0; i2 < jsonArray.length(); i2++) {
                    final InformationService iService = new InformationService();
                    iService.setRepositoryName(jsonArray.getJSONObject(i2).getString("name"));
                    iService.setRepositoryDescription(jsonArray.getJSONObject(i2).getString("description"));
                    iService.setForks(jsonArray.getJSONObject(i2).getInt("forks"));
                    iService.setStars(jsonArray.getJSONObject(i2).getInt("stargazers_count"));
                    JSONObject owner = jsonArray.getJSONObject(i2).getJSONObject("owner");
                    iService.setUserName(owner.getString("login"));
                    iService.setUserPhoto(owner.getString("avatar_url"));
                    iService.setPullUrl(jsonArray.getJSONObject(i2).getString("pulls_url"));

                    RequestQueue queue2 = Volley.newRequestQueue(ctx);
                    String url2 = owner.getString("url");


                    JsonObjectRequest jsObjRequest2 = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                iService.setFullName(response.getString("name"));
                            } catch (JSONException e) {

                                Log.e(TAG, "Error: " + e);
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            EventBus.getDefault().post(new ErroEvent(error.getMessage()));
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            String credentials = "leandro.spidalieri@gmail.com:erun2pbr";
                            String auth = "Basic "
                                    + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                            headers.put("Content-Type", "application/json");
                            headers.put("Authorization", auth);
                            return headers;
                        }
                    };


                    queue2.add(jsObjRequest2);
                    allRepositories.add(iService);

                }
                if(page == 1){
                    pDialog.dismiss();
                }
                EventBus.getDefault().post(new ItensEvent(allRepositories));


            } else {
                Log.e(TAG, "Sem conexão");
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error: " + e);
        }
    }
}
